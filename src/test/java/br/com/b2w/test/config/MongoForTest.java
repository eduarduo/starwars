package br.com.b2w.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories(basePackages = "br.com.b2w.repository")
public class MongoForTest extends AbstractMongoConfiguration {

	private static final String DB_NAME = "test_starwars";

	@Override
	protected String getDatabaseName() {
		return DB_NAME;
	}

	@Bean
	public Mongo mongo() {
		return new Fongo(getDatabaseName()).getMongo();
	}
	
	@Override
	protected String getMappingBasePackage() {
		return "br.com.b2w.model";
	}

	@Override
	public MongoClient mongoClient() {
		MongoClient mongoClient = new MongoClient();
		mongoClient.dropDatabase(DB_NAME);
		return mongoClient;
	}
}