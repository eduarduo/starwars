package br.com.b2w.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.jayway.jsonpath.JsonPath;

import br.com.b2w.config.StarwarsApplication;
import br.com.b2w.test.config.MongoForTest;
import br.com.b2w.test.tool.Constants;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { StarwarsApplication.class })
@AutoConfigureMockMvc
@ContextConfiguration(classes = MongoForTest.class)
public class PlanetControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldNotFindPlanetByEmptyName() throws Exception {
		mockMvc.perform(get("/planets/name/   ")).andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldNotFindPlanetByInvalidName() throws Exception {
		mockMvc.perform(get("/planets/name/marte")).andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldNotFindUnsavedPlanetByInvalidName() throws Exception {
		mockMvc.perform(get("/planets/name/skako")).andDo(print()).andExpect(status().isNotFound());
	}
	
	@Test
	public void shouldNotSaveEmptyPlanet() throws Exception {
		mockMvc.perform(post("/planets/").contentType(Constants.APPLICATION_JSON_UTF8).content("{ }")).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldNotSaveDuplicatedName() throws Exception {
		mockMvc.perform(
				post("/planets/")
					.contentType(Constants.APPLICATION_JSON_UTF8)
					.content("{ \"name\" : \"Serenno\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isCreated());

		mockMvc.perform(
				post("/planets/")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				.content("{ \"name\" : \"Serenno\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldNotSavePlanetWithInvalidName() throws Exception {
		
		mockMvc.perform(
				post("/planets/")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				.content("{ \"name\" : \"Jupiter\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.message").value("Planet dont exists in star wars saga."));
	}

	@Test
	public void shouldSavePlanet() throws Exception {
		
		mockMvc.perform(
				post("/planets/")
					.contentType(Constants.APPLICATION_JSON_UTF8)
					.content("{ \"name\" : \"Concord Dawn\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.name").value("Concord Dawn"))
		.andExpect(jsonPath("$.totalFilms").value("0"))
		.andExpect(jsonPath("$.id").isNotEmpty());
	}
	
	@Test
	public void shouldFindPlanetByName() throws Exception {
		mockMvc.perform(
				post("/planets/")
					.contentType(Constants.APPLICATION_JSON_UTF8)
					.content("{ \"name\" : \"Zolan\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isCreated());
		mockMvc.perform(
			get("/planets/name/zolan")
		)
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.*").isArray())
		.andExpect(jsonPath("$.length()").value("1"))
		.andExpect(jsonPath("$[0].name").value("Zolan"))
		.andExpect(jsonPath("$[0].id").isNotEmpty());
	}

	@Test
	public void shouldFindPlanetById() throws Exception {
		MvcResult mvcResult = mockMvc.perform(
				post("/planets/")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				.content("{ \"name\" : \"Jakku\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
		.andExpect(status().isCreated()).andReturn();
		
		String id = JsonPath.parse(mvcResult.getResponse().getContentAsString()).read("$.id");
		
		mockMvc.perform(
				get("/planets/" + id)
				)
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name").value("Jakku"))
		.andExpect(jsonPath("$.totalFilms").value(1))
		.andExpect(jsonPath("$.id").isNotEmpty())
		.andExpect(jsonPath("$.id").value(id));
	}
	
	
	@Test
	public void shouldNotFindPlanetByInvalidId() throws Exception {
		mockMvc.perform(
				get("/planets/123")
				)
		.andDo(print())
		.andExpect(status().isNotFound());
	}

	@Test
	public void shouldListAllPlanets() throws Exception {
		mockMvc.perform(
				post("/planets/")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				.content("{ \"name\" : \"Ojom\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
				.andExpect(status().isCreated());
		
		mockMvc.perform(
				get("/planets/")
				)
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.*").isArray())
		.andExpect(jsonPath("$.length()").isNumber());
	}
	
	@Test
	public void shouldGetErrorOnDeleteWithEmptyId() throws Exception {
		mockMvc.perform(
				delete("/planets/    ")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				)
		.andExpect(status().isBadRequest());
	}

	@Test
	public void shouldGetErrorOnDeleteWithInexistentId() throws Exception {
		mockMvc.perform(
			delete("/planets/123")
			.contentType(Constants.APPLICATION_JSON_UTF8)
			)
			.andExpect(status().isNotFound());
	}
	
	@Test
	public void shouldDeletePlanet() throws Exception {
		MvcResult mvcResult = mockMvc.perform(
				post("/planets/")
				.contentType(Constants.APPLICATION_JSON_UTF8)
				.content("{ \"name\" : \"Muunilinst\", \"terrain\": \"xxx\", \"climate\": \"xxx\" }"))
				.andExpect(status().isCreated()).andReturn();
				
		String id = JsonPath.parse(mvcResult.getResponse().getContentAsString()).read("$.id");
				
		mockMvc.perform(
				delete("/planets/" + id)
				)
		.andDo(print())
		.andExpect(status().isOk());
	}
}