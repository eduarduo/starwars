package br.com.b2w.test.tool;

import java.nio.charset.Charset;

import org.springframework.http.MediaType;

public interface Constants {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
}