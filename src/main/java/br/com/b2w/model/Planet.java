package br.com.b2w.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;

public class Planet {

	@Id
	private String id;

	@Indexed(unique = true)
	private String name;

	private String climate;
	private String terrain;

	@Transient
	private Integer totalFilms;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getTotalFilms() {
		return totalFilms;
	}

	public void setTotalFilms(Integer totalFilms) {
		this.totalFilms = totalFilms;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

}