package br.com.b2w.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.b2w.model.Planet;

public interface PlanetRepository extends MongoRepository<Planet, String> {

	List<Planet> findByNameIgnoreCase(String name);

	boolean existsByNameIgnoreCase(String name);

}