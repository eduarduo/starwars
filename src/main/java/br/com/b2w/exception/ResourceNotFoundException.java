package br.com.b2w.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1717269577197951989L;
	public ResourceNotFoundException(String msg) {
		super(msg);
	}

}
