package br.com.b2w.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerErrorException extends RuntimeException {

	private static final long serialVersionUID = 2517068992062736672L;

	public ServerErrorException(String msg) {
		super(msg);
	}
}