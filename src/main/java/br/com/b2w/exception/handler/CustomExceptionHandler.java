package br.com.b2w.exception.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.b2w.exception.BadRequestException;
import br.com.b2w.exception.ResourceNotFoundException;
import br.com.b2w.exception.ServerErrorException;
import br.com.b2w.vo.CustomError;

@ControllerAdvice
@RestController
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<CustomError> handleResourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
		CustomError error = new CustomError(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<CustomError> handleBadRequestException(BadRequestException ex, WebRequest request) {
		CustomError error = new CustomError(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ServerErrorException.class)
	public final ResponseEntity<CustomError> handleServerErrorException(ServerErrorException ex, WebRequest request) {
		CustomError error = new CustomError(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}