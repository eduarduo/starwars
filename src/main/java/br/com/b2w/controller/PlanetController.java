package br.com.b2w.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.b2w.model.Planet;
import br.com.b2w.service.PlanetService;
import br.com.b2w.vo.PlanetIn;

/**
 * 
 * Controller responsible for Star Wars planets requests
 * 
 * @author eduarduo@gmail.com
 *
 */
@RestController
@RequestMapping("/planets")
public class PlanetController {

	@Autowired
	private PlanetService service;


	@RequestMapping(method = RequestMethod.GET, params= {})
	public ResponseEntity<List<Planet>> findAll() {
		return new ResponseEntity<>(service.get(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public ResponseEntity<Planet> getById(@PathVariable String id) {
		return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/name/{name}")
	public ResponseEntity<List<Planet>> getByName(@PathVariable String name){
	    return new ResponseEntity<>(service.findByName(name), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Planet> save(@RequestBody PlanetIn planet) {
		return new ResponseEntity<>(service.save(planet), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public ResponseEntity<Boolean> delete(@PathVariable String id) {
		return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
	}
}