package br.com.b2w.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StarWarsApiResponse {

	private String next;
	private List<StarWarsPlanet> results;
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public List<StarWarsPlanet> getResults() {
		return results;
	}
	public void setResults(List<StarWarsPlanet> results) {
		this.results = results;
	}
}