package br.com.b2w.vo;

import br.com.b2w.model.Planet;

public class PlanetIn {
	private String name;
	private String climate;
	private String terrain;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public Planet toModel() {
		Planet planet = new Planet();
		planet.setClimate(this.climate);
		planet.setName(this.name);
		planet.setTerrain(this.terrain);
		return planet ;
	}

}
