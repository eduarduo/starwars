package br.com.b2w.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.b2w.exception.BadRequestException;
import br.com.b2w.exception.ResourceNotFoundException;
import br.com.b2w.model.Planet;
import br.com.b2w.repository.PlanetRepository;
import br.com.b2w.vo.PlanetIn;

@Service
public class PlanetService extends ServiceBase {

	@Autowired
	private StarWarsApiService starWarsApi;

	@Autowired
	private PlanetRepository repository;

	public List<Planet> get() {
		List<Planet> planets = repository.findAll();
		for (Planet planet : planets) {
			planet.setTotalFilms(starWarsApi.getTotalByName(planet.getName()));
		}
		return planets;
	}

	public List<Planet> findByName(String name) {
		if ( StringUtils.isBlank(name) ) {
			throw new BadRequestException("Field name is required.");
		}
		if ( !starWarsApi.existsByName(name) ) {
			throw new BadRequestException("Planet dont exists in star wars saga.");
		}
		List<Planet> items = repository.findByNameIgnoreCase(name);
		if ( items.isEmpty() ) {
			throw new ResourceNotFoundException("There is no item with name: " + name);
		}
		return items;
	}

	public Planet save(PlanetIn planetIn) {

		checkRequired(planetIn.getName(), "Field name is required.");
		checkRequired(planetIn.getClimate(), "Field climate is required.");
		checkRequired(planetIn.getTerrain(), "Field terrain is required.");

		if ( !starWarsApi.existsByName(planetIn.getName()) ) {
			throw new BadRequestException("Planet dont exists in star wars saga.");
		}
		
		if ( repository.existsByNameIgnoreCase(planetIn.getName()) ) {
			throw new BadRequestException("Planet already exists.");
		}
		Planet planet = repository.save(planetIn.toModel());
		planet.setTotalFilms(starWarsApi.getTotalByName(planet.getName()));
		return planet;
	}

	public Planet findById(String id) {
		checkRequired(id, "Field id is required.");
		Optional<Planet> optionalPlanet = repository.findById(id);
		if ( optionalPlanet.isPresent() ) {
			Planet planet = optionalPlanet.get();
			Integer total = starWarsApi.getTotalByName(planet.getName());
			planet.setTotalFilms(total);
			return planet;
			
		}
		throw new ResourceNotFoundException("There is no item with id: " + id);
	}

	public boolean delete(String id) {
		checkRequired(id, "Field id is required.");
		Optional<Planet> optionalPlanet = repository.findById(id);
		if ( optionalPlanet.isPresent() ) {
			repository.deleteById(id);
			return true;
		}
		throw new ResourceNotFoundException("There is no item with id: " + id);
	}
}