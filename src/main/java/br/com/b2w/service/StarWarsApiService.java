package br.com.b2w.service;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import br.com.b2w.exception.BadRequestException;
import br.com.b2w.exception.ServerErrorException;
import br.com.b2w.vo.StarWarsApiResponse;

/**
 * 
 * Service layer 
 *
 */
@Service
public class StarWarsApiService extends ServiceBase {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${starwars-api-path}")
	private String URL_SW_API;
    
	/**
	 * Guava cache to mantain in memory all the planets from swapi.
	 *
	 */
	private final LoadingCache<Object, Map<String, Integer>> cache = CacheBuilder.newBuilder().build(
	new CacheLoader<Object, Map<String, Integer>>() {
		public Map<String, Integer> load(Object key) {
			// Initial load with all items 
			return findAll();
		}
	});
	

	/**
	 * Retrieves recursively the planets from the external rest api
	 * 
	 * @return Map<String, Integer>
	 */
	public Map<String, Integer> findAll() {
		return getAllPages(URL_SW_API);
	}

	private Map<String, Integer> getAllPages(String url) {
		if ( !ResourceUtils.isUrl(url) ) {
			throw new BadRequestException("URL rest inválida : " + url);
		}
		StarWarsApiResponse response = restTemplate.getForObject(url, StarWarsApiResponse.class);
		Map<String, Integer> results = response.getResults().stream().collect(Collectors.toMap( p -> p.getName().toUpperCase(), p -> p.getFilms().size()));
		
		if (!StringUtils.isEmpty(response.getNext())) {
			Map<String, Integer> nextPage = getAllPages(response.getNext());

			Map<String, Integer> merged = 
				Stream.of(results, nextPage)
		        .map(Map::entrySet)
		        .flatMap(Collection::stream)
		        .collect(
		            Collectors.toMap(
		                Map.Entry::getKey,
		                Map.Entry::getValue,
		                Integer::max
		            )
		        );
			results = merged;
			
		}
		return results;
	}

	public Integer getTotalByName(String name) {
		checkRequired(name, "Field name is required.");
		try {
			return cache.get("map").get(name.toUpperCase());
		} catch (ExecutionException e) {
			throw new ServerErrorException("Error while cache reading.");
		}
	}

	public boolean existsByName(String name) {
		return getTotalByName(name) != null;
	}
}