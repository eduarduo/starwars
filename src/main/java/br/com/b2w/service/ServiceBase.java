package br.com.b2w.service;

import org.apache.commons.lang3.StringUtils;

import br.com.b2w.exception.BadRequestException;

public class ServiceBase {

	protected void checkRequired(String value, String message) {
		if ( StringUtils.isBlank(value) ) {
			throw new BadRequestException(message);
		}
	}
	
}
